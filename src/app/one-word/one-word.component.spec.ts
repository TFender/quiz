import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneWordComponent } from './one-word.component';

describe('PauzeComponent', () => {
  let component: OneWordComponent;
  let fixture: ComponentFixture<OneWordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneWordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneWordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
