import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'one-word',
  templateUrl: './one-word.component.html',
  styleUrls: ['./one-word.component.css']
})
export class OneWordComponent implements OnInit {

  constructor() { }

  @Input() word = 'word';

  ngOnInit(): void {
  }

}
