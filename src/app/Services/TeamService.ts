import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Team} from '../DTO/Team';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TeamService {


  constructor(private httpClient: HttpClient) {
  }

  public async GetTeams() {
    const asyncResult = await this.httpClient.get<Team[]>('http://localhost:3000/read').toPromise();
    const rawTeams = this.convertTeams(asyncResult);
    const teams: Team[] = [];
    rawTeams.forEach(p => {
      teams.push(new Team(p.Name, p.Scores));
    });
    return teams;
  }

  public async PostTeams(teams: Team[]) {
    await this.httpClient.post('http://localhost:3000/write',
      {Teams: teams},
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }).toPromise();
  }

  private convertTeams(data) {
    return data.Teams;
  }
}
