import {RankStatus} from './RankStatus';

export class Team {

  Name = 'empty';
  Scores: number[] = [];

  public rank: number;
  public previousrank: number;
  public previousScore: number;
  public currentScore: number;
  public rankStatus: RankStatus;

  constructor(name: string, scores: number[]) {
    this.Name = name;
    this.Scores = scores;
  }

  public Sum(): number {
    let sum = 0;
    this.Scores.forEach(p => sum += p);
    return sum;
  }

  public SumPerRound(): number[] {
    const sumPerRound: number[] = [];

    for (let i = 0; i < this.Scores.length; i++) {
      let sum = 0;
      for (let j = 0; j <= i; j++) {
        sum += this.Scores[j];
      }
      sumPerRound.push(sum);
    }

    return sumPerRound;
  }

  SetRound(event: any, roundNumber: number) {
    this.Scores[roundNumber] = parseInt(event.target.value, 10);
  }
}
