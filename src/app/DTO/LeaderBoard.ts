import {Team} from './Team';
import {RankStatus} from './RankStatus';
import {LeaderBoardRow} from './LeaderBoardRow';

export class LeaderBoard {

  public Rows: LeaderBoardRow[] = [];

  public SetLeaderBoardRows(teams: Team[]) {
    this.Rows = [];
    if (teams[0].Scores.length > 1) {

      teams.sort((a, b) => {
        a.previousScore = a.SumPerRound()[a.Scores.length - 2];
        b.previousScore = b.SumPerRound()[b.Scores.length - 2];
        return b.previousScore - a.previousScore;
      });

      let prevPrevScore = 0;
      let prevRank = 1;
      teams.forEach((p, i) => {
        if (i !== 0) {
          if (p.previousScore !== prevPrevScore) {
            prevRank++;
          }
        }
        prevPrevScore = p.previousScore;
        p.previousrank = prevRank;
      });
    }

    teams.sort((a, b) => {
      a.currentScore = a.Sum();
      b.currentScore = b.Sum();
      return b.currentScore - a.currentScore;
    });

    let prevScore = 0;
    let rank = 1;
    teams.forEach((p, i) => {
      if (i !== 0) {
        if (p.currentScore !== prevScore) {
          rank++;
        }
      }
      prevScore = p.currentScore;
      p.rank = rank;
    });

    if (teams[0].Scores.length > 1) {
      teams.forEach((p, i) => {
        if (p.rank < p.previousrank) {
          p.rankStatus = RankStatus.Rise;
        } else if (p.rank === p.previousrank) {
          p.rankStatus = RankStatus.Stagnant;
        } else {
          p.rankStatus = RankStatus.Fall;
        }
      });
    }
    teams.forEach((p, i) => {
      const amount = p.previousrank - p.rank;
      this.Rows.push(new LeaderBoardRow(p.Name, p.currentScore, p.rank, p.rankStatus, amount));
    });
  }
}

