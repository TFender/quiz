import {RankStatus} from './RankStatus';

export class LeaderBoardRow {
  Name: string;
  Score: number;
  Rank: number;
  RankStatus: RankStatus;
  Amount: number;

  constructor(name: string, score: number, rang: number, rankStatus: RankStatus, amount: number) {
    this.Name = name;
    this.Score = score;
    this.Rank = rang;
    this.RankStatus = rankStatus;
    this.Amount = amount;
  }
}
