import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AppComponent} from './app.component';
import {BarGraphComponent} from './pauze/bar-graph/bar-graph.component';
import {InputComponent} from './input/input.component';
import {HttpClientModule} from '@angular/common/http';
import {LeaderBoardComponent} from './pauze/leaderboard/leader-board.component';
import {PauzeComponent} from './pauze/pauze.component';
import {LineGraphComponent} from './pauze/line-graph/line-graph.component';
import {TeamService} from './Services/TeamService';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { OneWordComponent } from './one-word/one-word.component';
import { LeaderBoardShowComponent } from './leaderboardshow/leader-board-show.component';
import { MomentComponent } from './moment/moment.component';
import { SuccesComponent } from './succes/succes.component';

const appRoutes: Routes = [
  {path: 'succes', component: SuccesComponent},
  {path: 'moment', component: MomentComponent},
  {path: 'stand', component: LeaderBoardShowComponent},
  {path: 'pauze', component: PauzeComponent},
  {path: 'input', component: InputComponent},
  {
    path: '**',
    redirectTo: '/pauze',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    BarGraphComponent,
    InputComponent,
    LeaderBoardComponent,
    PauzeComponent,
    LineGraphComponent,
    OneWordComponent,
    LeaderBoardShowComponent,
    MomentComponent,
    SuccesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes
    ),
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [TeamService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
