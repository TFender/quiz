import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PauzeComponent } from './pauze.component';

describe('OutputComponent', () => {
  let component: PauzeComponent;
  let fixture: ComponentFixture<PauzeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PauzeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PauzeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
