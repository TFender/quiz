import { Component, OnInit } from '@angular/core';
import {Team} from '../../DTO/Team';
import * as CanvasJS from '../../../assets/canvasjs.min';
import {TeamService} from '../../Services/TeamService';

@Component({
  selector: 'app-line-graph',
  templateUrl: './line-graph.component.html',
  styleUrls: ['./line-graph.component.css']
})
export class LineGraphComponent implements OnInit {

  private teams: Team[] = [];
  private loaded = false;
  public height: number;

  constructor(private teamServive: TeamService) {
  }

  ngOnInit() {
    this.height = window.innerHeight / 2 - 10;
    this.GetTeams();
  }


  private async GetTeams() {
    this.teams = await this.teamServive.GetTeams();
    this.teams.sort((a, b) => a.Sum() - b.Sum());
    this.GetGrafiek();
  }

  private GetGrafiek() {
    const chart = new CanvasJS.Chart('grafiek', {
      animationEnabled: true,
      axisY: {
        interlacedColor: 'Azure',
        interval: 5,
        tickColor: 'lightblue',
        gridColor: 'lightblue',
        tickLength: 10
      },
      data: this.TeamsToDataGraph()
    });

    chart.render();
    this.loaded = true;
  }

  private TeamsToDataGraph() {
    const data = [];
    this.teams.forEach(p =>
      data.push(
        {
          type: 'line',
          visible: true,
          showInLegend: true,
          legendMarkerType: 'circle',
          name: p.Name,
          dataPoints: this.SumPerRoundToDataPoints(p)
        }
      ));
    console.log(data);
    return data;
  }

  private SumPerRoundToDataPoints(team: Team) {
    const dataPoint = [];
    const sumPerRound = team.SumPerRound();
    sumPerRound.forEach((p, i) => {
      dataPoint.push({y: p, label: 'Ronde ' + (i + 1)});
    });
    return dataPoint;
  }

}
