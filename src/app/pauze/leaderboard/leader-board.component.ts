import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Team} from '../../DTO/Team';
import {LeaderBoard} from '../../DTO/LeaderBoard';
import {TeamService} from '../../Services/TeamService';

@Component({
  selector: 'app-leader-board',
  templateUrl: './leader-board.component.html',
  styleUrls: ['./leader-board.component.css'],
})
export class LeaderBoardComponent implements OnInit {

  private teams: Team[] = [];
  public leaderBoard: LeaderBoard;
  public loaded: boolean;

  constructor(private teamService: TeamService) {
  }

  ngOnInit() {
    this.GetTeams();
  }


  private async GetTeams() {
    this.teams = await this.teamService.GetTeams();
    this.leaderBoard = new LeaderBoard();
    this.leaderBoard.SetLeaderBoardRows(this.teams);
    this.loaded = true;
    this.start();
  }

  async start() {
    await new Promise(resolve => setTimeout(resolve, 50));
    document.getElementById('list').classList.add('active');
  }

}
