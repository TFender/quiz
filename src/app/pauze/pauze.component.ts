import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-pauze',
  templateUrl: './pauze.component.html',
  styleUrls: ['./pauze.component.css']
})
export class PauzeComponent implements OnInit {

  public slideNumber = 0;
  public slidesMax = 4;

  constructor() {
  }

  ngOnInit(): void {
    setInterval(() => {
      this.slideNumber = (this.slideNumber + 1) % this.slidesMax;
    }, 12000);
  }

}
