import {Component, OnInit} from '@angular/core';
import {Team} from '../../DTO/Team';
import * as CanvasJS from '../../../assets/canvasjs.min';
import {TeamService} from '../../Services/TeamService';

@Component({
  selector: 'app-bar-graph',
  templateUrl: './bar-graph.component.html',
  styleUrls: ['./bar-graph.component.css']
})
export class BarGraphComponent implements OnInit {

  private teams: Team[] = [];
  public height: number;

  constructor(private teamService: TeamService) {
  }

  ngOnInit() {
    this.height = window.innerHeight / 2 - 10;
    this.getTeams();
  }


  private async getTeams() {
    this.teams = await this.teamService.GetTeams();
    this.teams.sort((a, b) => a.Sum() - b.Sum());
    this.getStaafDiagram();
  }

  private getStaafDiagram() {
    const data = this.TeamsToDataPoints();
    const chart = new CanvasJS.Chart('staaf-diagram', {
      animationEnabled: true,
      axisY: {
        interlacedColor: 'Azure',
        interval: 5,
        tickColor: 'lightblue',
        gridColor: 'lightblue',
        tickLength: 10
      },
      axisX: {
        valueFormatString: ' ',
        tickLength: 0
      },
      data: [{
        type: 'column',
        dataPoints: data
      }]
    });

    chart.render();
  }

  private TeamsToDataPoints() {
    const dataPoint = [];
    this.teams.forEach(p => {
      dataPoint.push({y: p.Sum(), indexLabel: p.Name, indexLabelPlacement: 'inside', indexLabelOrientation: 'vertical'});
    });
    return dataPoint;
  }

}
