import {Component, OnInit} from '@angular/core';
import {Team} from '../DTO/Team';
import {TeamService} from '../Services/TeamService';
import {LeaderBoard} from '../DTO/LeaderBoard';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  public teams: Team[] = [];
  public loaded = false;

  private leaderBoard = new LeaderBoard();

  constructor(private teamsService: TeamService) {
  }

  ngOnInit(): void {
    this.getTeams();
  }

  private async getTeams() {
    this.teams = await this.teamsService.GetTeams();
    this.leaderBoard.SetLeaderBoardRows(this.teams);
    this.loaded = true;
  }

  public addRound() {
    this.teams.forEach(p => p.Scores.push(0));
  }

  public removeRound() {
    this.teams.forEach(p => {
      if (p.Scores.length > 0) {
        p.Scores.splice(-1, 1);
      }
    });
  }

  public addTeam() {
    const rounds = this.teams.length > 0 ? this.teams[0].Scores.length : 1;
    const array: number[] = [];
    for (let i = 0; i < rounds; i++) {
      array.push(0);
    }
    this.teams.push(new Team('', array));
  }

  public removeTeam(team: Team) {
    const index = this.teams.indexOf(team);
    if (index > -1) {
      this.teams.splice(index, 1);
    }
  }

  public async submit() {
    await this.teamsService.PostTeams(this.teams);
    this.loaded = false;
    this.getTeams();
  }


}
