import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaderBoardShowComponent } from './leader-board-show.component';

describe('LeadersboardshowComponent', () => {
  let component: LeaderBoardShowComponent;
  let fixture: ComponentFixture<LeaderBoardShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaderBoardShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaderBoardShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
