import {Component, OnInit} from '@angular/core';
import {Team} from '../DTO/Team';
import {LeaderBoard} from '../DTO/LeaderBoard';
import {TeamService} from '../Services/TeamService';
import {LeaderBoardRow} from '../DTO/LeaderBoardRow';

@Component({
  selector: 'app-leaderboard-show',
  templateUrl: './leader-board-show.component.html',
  styleUrls: ['./leader-board-show.component.css']
})
export class LeaderBoardShowComponent implements OnInit {

  private teams: Team[] = [];
  public leaderBoardRows: LeaderBoardRow[] = [];
  public leaderBoard: LeaderBoard;
  public loaded: boolean;
  private started: boolean;

  constructor(private teamService: TeamService) {
  }

  ngOnInit() {
    this.GetTeams();
    window.addEventListener('click', this.nextItem.bind(this));
  }


  private async GetTeams() {
    this.teams = await this.teamService.GetTeams();
    this.leaderBoard = new LeaderBoard();
    this.leaderBoard.SetLeaderBoardRows(this.teams);
    this.loaded = true;
    this.start();
  }

  async start() {
    await new Promise(resolve => setTimeout(resolve, 50));
    this.started = true;
  }

  public nextItem(event) {
    if (!this.started) {
      return;
    }

    if (this.leaderBoard.Rows.length === 0){
      this.tearDown();
      return;
    }

    let rank = 0;
    this.leaderBoard.Rows.forEach(p => {
      if (p.Rank > rank) {
        rank = p.Rank;
      }
    });
    let highestIndex = 0;
    for (const [index, p] of this.leaderBoard.Rows.entries()) {
      if (p.Rank === rank) {
        highestIndex = index;
        break;
      }
    }
    const deleteCount = this.leaderBoard.Rows.length - highestIndex;
    const rows = this.leaderBoard.Rows.splice(highestIndex, deleteCount);
    rows.forEach(p => this.leaderBoardRows.splice(0, 0, p));

    this.zoom(rank);
  }

  private async zoom(rank: number) {
    console.log(rank);
    await new Promise(resolve => setTimeout(resolve, 10));
    const items = document.getElementsByClassName('rank' + rank);
    for (let i = 0; i < items.length; i++) {
      items[i].classList.add('active');
      setTimeout(() => items[i].classList.remove('active'), 4000);
    }
  }

  private tearDown() {
    this.started = false;
    this.leaderBoardRows = [];
    this.GetTeams();
  }

}
