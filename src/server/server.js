const fs = require('fs');
var express = require("express");
var bodyParser = require('body-parser');
var app = express();

var filename = "quiz0204.json";

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(bodyParser.json());

app.listen(3000, () => {
  console.log("Server running on port 3000");
});

function writeToJson(jsonData)
{
  console.log("Write call received!");
// stringify JSON Object
  var jsonContent = JSON.stringify(jsonData);
  console.log(jsonContent);

  fs.writeFile(filename, jsonContent, 'utf8', function (err) {
    if (err) {
      console.log("An error occured while writing JSON Object to File.");
      return console.log(err);
    }

    console.log("JSON file has been saved.");
  });
}

function readFromJson(){
  console.log("Read call received!");
  let rawdata = fs.readFileSync(filename);
  let scores = JSON.parse(rawdata);
  console.log(scores);
  return scores;
}

app.get('/read', (req, res) => res.send(readFromJson()));
app.post('/write',function(req,res){
  writeToJson(req.body);
  res.status(200).json({status:"ok"})
});
